package com.fun;

import java.io.Console;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Sudoku solver class that reads puzzle input from local file and solves and prints all solutions
 * one at a time upon user prompts.
 */
public class SudokuSolver {
  // Colorful printing of puzzle and solutions.
  private static final String ANSI_RESET = "\u001B[0m";
  private static final String ANSI_RED = "\u001B[31m";
  private static final String ANSI_GREEN = "\u001B[32m";

  // The 9x9 squares.
  private final Integer[][] value = new Integer[9][9];

  // Whether a square was originally set by the puzzle input.
  private final boolean[][] originallySet = new boolean[9][9];

  // The list of all initial blanks to be solved.
  private List<Blank> blanksToSolve = new ArrayList<>();

  /**
   * Main program, expecting the only argument to be the path to the puzzle input text file.
   *
   * <p> The input file must have 9 lines, each of which is a space-delimited string. Each component
   * must be either a digit 1-9 or a non-digit character to represent unknown blank.
   *
   * <p> For example, the following is an example of a valid input file.
   *
   *     _ 1 9 _ 5 _ 3 2 _
   *     _ 8 5 _ _ _ _ _ _
   *     7 _ _ _ 9 _ _ _ 1
   *     4 _ _ 5 _ _ 9 _ _
   *     _ _ 2 _ 7 _ 5 _ _
   *     _ _ 8 _ _ 1 _ _ 3
   *     1 _ _ _ 4 _ _ _ 6
   */
  public static void main(String[] args) {
    if (args.length != 1) {
      System.err.println("usage: sudoku <input_file>");
      System.exit(1);
    }
    try {
      SudokuSolver solver = SudokuSolver.loadPuzzle(args[0]);
      solver.promptForNext();
      solver.solve();
    } catch (IOException e) {
      System.err.println("Failed to load file: " + e.getMessage());
      System.exit(1);
    }
  }

  /**
   * Loads the {@link SudokuSolver} object from the given {@code inputFile}.
   *
   * @param inputFile the path to the puzzle input text file.
   *
   * @throws IOException if {@code inputFile} cannot be loaded.
   *
   * @returns the {@link SudokuSolver} object initially loaded with the puzzle.
   */
  public static SudokuSolver loadPuzzle(String inputFile) throws IOException {
    List<String> lines = Files.readAllLines(
        FileSystems.getDefault().getPath(inputFile),
        Charset.defaultCharset());

    // Verify input line count.
    if (lines.size() != 9) {
      throw new IllegalArgumentException(
          "Input file '" + inputFile + "' has " + lines.size() + " lines, expecting 9 lines.");
    }

    SudokuSolver solver = new SudokuSolver();

    for (int y = 0; y < 9; ++y) {
      String[] parts = lines.get(y).split(" ");
      // Verify each line has 9 parts, delimited by spaces.
      if (parts.length != 9) {
        throw new IllegalArgumentException(
            "Line #" + (y + 1) + " has " + parts.length + " parts, expecting 9 parts.");
      }
      // Parse each component.
      for (int x = 0; x < 9; ++x) {
        try {
          solver.value[y][x] = Integer.valueOf(parts[x]);
          solver.originallySet[y][x] = true;
        } catch (NumberFormatException e) {
          // A NaN means blank.
          solver.value[y][x] = null;
          solver.originallySet[y][x] = false;
        }
      }
    }
    // Initialize internal data members.
    solver.initialize();

    System.out.println("\n=====LOADED PUZZLE=====\n");
    // Prints the original puzzle upfront.
    solver.printToConsole();
    return solver;
  }

  /**
   * Solves the loaded Sudoku puzzle, and prints all the solutions one by one upon user prompts.
   */
  public void solve() {
    // Invoke recursion entry point.
    solveRecursive(blanksToSolve);

    System.out.println("\n======== ALL SOLUTIONS FOUND =========");
  }

  /**
   * Waits for user inputs from the console. "y" or ENTER means continue to show the next solution,
   * "n" or Ctrl-D means quit.
   */
  public static void promptForNext() {
    Console console = System.console();
    while (true) {
      String searchNext = console.readLine("Search for next solution (Y/n)? ");
      if (searchNext == null || searchNext.toLowerCase().equals("n")) {
        System.out.println("Bye...");
        System.exit(0);
      }
      if (searchNext.toLowerCase().equals("y") || searchNext.isEmpty()) {
        return;
      }
    }
  }

  // Recursion entrance to run the solver for all the {@code remainingBlanks}.
  private void solveRecursive(List<Blank> remainingBlanks) {
    // Sort the remaining blanks by the size of their candidate digits (See Blank.compareTo()
    // below). This is to simulate how a person would have attacked the problem.
    Collections.sort(remainingBlanks);

    // No more remaining blanks to be solved, got one solution.
    if (remainingBlanks.isEmpty()) {
      verifySolution();
      System.out.println("\n=======SOLUTION=======\n");
      printToConsole();
      // Wait until user asks for the next one.
      promptForNext();
      return;
    }

    // The blank with the least candidate digits.
    Blank first = remainingBlanks.get(0);
    for (int digit : first.candidates) {
      // Try this candidate digit.
      value[first.y][first.x] = digit;

      ArrayList<Blank> nextRemainingBlanks = new ArrayList<>();
      // Subsets of remaining blanks which temporarily removed the digits for subsequent recursion.
      HashSet<Blank> needRestore = new HashSet<>();

      for (int i = 1; i < remainingBlanks.size(); ++i) {
        Blank blank = remainingBlanks.get(i);
        // If a remaining blank is on the same row, column or 3x3 block as 'first', make sure
        // 'digit' is not used as a candidate for 'blank' for the subsequent recursion call.
        if ((first.x == blank.x || first.y == blank.y
             || (first.x / 3 == blank.x / 3 && first.y / 3 == blank.y / 3))
            && blank.candidates.remove(digit)) {
          needRestore.add(blank);
        }
        nextRemainingBlanks.add(blank);
      }

      // Invoke recursion on a smaller number of remaining blanks.
      solveRecursive(nextRemainingBlanks);

      // Now restore digit's candidacy for blank, if applicable.
      for (Blank blank : needRestore) {
        blank.candidates.add(digit);
      }
    }

    // Restore global state, not absolutely necessary, but clean to do so.
    value[first.y][first.x] = null;
  }

  // Prints colorful Sudoku puzzle or solution to the console.
  private void printToConsole() {
    StringBuilder builder = new StringBuilder();
    for (int y = 0; y < 9; ++y) {
      for (int x = 0; x < 9; ++x) {
        if (originallySet[y][x]) {
          // Digit is provided in the original puzzle, print in vanilla white.
          builder.append(value[y][x]);
        } else if (value[y][x] == null) {
          // Value not set, print a red '?'.
          builder.append(ANSI_RED + "?" + ANSI_RESET);
        } else {
          // Value is set, but not original, must be from a solution, print in green.
          builder.append(ANSI_GREEN + value[y][x] + ANSI_RESET);
        }

        // Digit separator or EOL.
        builder.append(x == 8 ? '\n' : ' ');

        // Vertical separator between 3x3 blocks for nicer viewing.
        if (x == 2 || x == 5) {
          builder.append("| ");
        }
      }
      // Horizontal separator between 3x3 blocks for nicer viewing.
      if (y == 2 || y == 5) {
        builder.append("---------------------\n");
      }
    }
    System.out.println(builder.toString());
  }

  // Constructs an Integer array of size 9 given the column index.
  private Integer[] getColumn(int x) {
    return new Integer[] {
      value[0][x], value[1][x], value[2][x],
      value[3][x], value[4][x], value[5][x],
      value[6][x], value[7][x], value[8][x],
    };
  }

  // Constructs an Integer array of size 9 given the 3x3 block's coordinate.
  private Integer[] getBlock(int bx, int by) {
    int x = 3 * bx;
    int y = 3 * by;
    return new Integer[] {
      value[y + 0][x + 0], value[y + 0][x + 1], value[y + 0][x + 2],
      value[y + 1][x + 0], value[y + 1][x + 1], value[y + 1][x + 2],
      value[y + 2][x + 0], value[y + 2][x + 1], value[y + 2][x + 2],
    };
  }

  // Initializes the Sudoku puzzle by enumerating each blank's candidate digits.
  private void initialize() {
    // Missing digits for each row.
    ArrayList<Set<Integer>> rowCandidates = new ArrayList<>(9);
    for (int y = 0; y < 9; ++y) {
      rowCandidates.add(initializeGroup(value[y], "Row #" + y));
    }

    // Missing digits for each column.
    ArrayList<Set<Integer>> columnCandidates = new ArrayList<>(9);
    for (int x = 0; x < 9; ++x) {
      columnCandidates.add(initializeGroup(getColumn(x), "Column #" + x));
    }

    // Missing digits for each 3x3 block.
    ArrayList<Set<Integer>> blockCandidates = new ArrayList<>(9);
    for (int by = 0; by < 3; ++by) {
      for (int bx = 0; bx < 3; ++bx) {
        blockCandidates.add(
            initializeGroup(getBlock(bx, by), "Block (" + bx + ", " + by + ")"));
      }
    }

    // Now each blank's candidate is the set intersection of the candidates of its row, column
    // and 3x3 block.
    for (int y = 0; y < 9; ++y) {
      for (int x = 0; x < 9; ++x) {
        // Only extract candidate digits for the original blank.
        if (value[y][x] == null) {
          blanksToSolve.add(new Blank(
                x, y, rowCandidates.get(y), columnCandidates.get(x),
                blockCandidates.get(y / 3 * 3 + x / 3)));
        }
      }
    }
  }

  // For an Integer array of size 9, see if the present digits are valid for the puzzle.
  // Returns the set of missing digits in this group.
  private static Set<Integer> initializeGroup(Integer[] nineInts, String prefix) {
    boolean[] seen = new boolean[9];
    for (Integer n : nineInts) {
      if (n == null) {
        // Nulls are missing digits.
        continue;
      }
      if (n < 1 || n > 9) {
        throw new IllegalArgumentException(prefix + " has invalid integer " + n);
      }
      // Remember to offset the index by 1!!
      if (seen[n - 1]) {
        throw new IllegalArgumentException(prefix + " has duplicated integer " + n);
      }
      seen[n - 1] = true;
    }
    HashSet<Integer> candidates = new HashSet<>();
    for (int i = 0; i < 9; ++i) {
      if (!seen[i]) {
        candidates.add(i + 1);
      }
    }
    return candidates;
  }

  // Verifies that the current state represents a valid solution, otherwise throws Runtime to
  // indicate a bug of the algorithm (ideally should never happen).
  private void verifySolution() {
    for (int y = 0; y < 9; ++y) {
      verifyOneToNine(value[y], "Row #" + y);
    }
    for (int x = 0; x < 9; ++x) {
      verifyOneToNine(getColumn(x), "Column #" + x);
    }
    for (int by = 0; by < 3; ++by) {
      for (int bx = 0; bx < 3; ++bx) {
        verifyOneToNine(getBlock(bx, by), "Block (" + bx + ", " + by + ")");
      }
    }
  }

  // Verifies the Integer array of size 9 consists of a permutation of digit 1-9.
  private void verifyOneToNine(Integer[] nineInts, String prefix) {
    boolean[] seen = new boolean[9];
    for (Integer n : nineInts) {
      if (n == null) {
        throw new IllegalStateException(prefix + " has null integer");
      }
      if (n < 1 || n > 9) {
        throw new IllegalStateException(prefix + " has invalid integer " + n);
      }
      if (seen[n - 1]) {
        throw new IllegalStateException(prefix + " has duplicated integer " + n);
      }
      seen[n - 1] = true;
    }
  }

  /**
   * Internal representation of a blank in the original puzzle. It implements {@link Comparable} so
   * a natural ordering is defined in such a way that the blank with fewer candidate digits is
   * considered 'smaller'. When two blanks have the same number of candidates, use their coordinate
   * to break the tie.
   */
  private static class Blank implements Comparable<Blank> {
    // Coordinates in the 9x9 board.
    private final int x;
    private final int y;

    // The candidate digit this Blank is allowed to take, this variable changes during the time of
    // SudokuSolver.solve().
    private HashSet<Integer> candidates;

    public Blank(
        int x, int y, Set<Integer> rowCandidates,
        Set<Integer> columnCandidates, Set<Integer> blockCandidates) {
      this.x = x;
      this.y = y;
      // 3-way intersection determines the blank's candidate digits.
      candidates = new HashSet<>(rowCandidates);
      candidates.retainAll(columnCandidates);
      candidates.retainAll(blockCandidates);
    }

    @Override
    public int compareTo(Blank other) {
      // Candidate with smaller size yields a smaller blank.
      if (this.candidates.size() < other.candidates.size()) {
        return -1;
      }
      if (this.candidates.size() > other.candidates.size()) {
        return 1;
      }
      // Use coordinates to break ties.
      if (this.y < other.y) {
        return -1;
      }
      if (this.y > other.y) {
        return 1;
      }
      if (this.x < other.x) {
        return -1;
      }
      if (this.x > other.x) {
        return 1;
      }
      return 0;
    }
  }
}
